// coverage: off
#[derive(Debug)]
pub enum FailureMode {
    VarError(std::env::VarError),
    GitlabError(gitlab::GitlabError),
}

impl std::fmt::Display for FailureMode {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            FailureMode::VarError(inner) => {
                write!(f, "Error reading $GITLAB_PAT: {}", inner)
            }
            FailureMode::GitlabError(inner) => {
                write!(
                    f,
                    "Error connecting to GitLab: {}. (Is your $GITLAB_PAT correct?)",
                    inner
                )
            }
        }
    }
}

/// Creates a connection to gitlab.com using the provided GitLab PAT.
// coverage: on
pub fn connect() -> Result<gitlab::Gitlab, FailureMode> {
    let token = match std::env::var("GITLAB_PAT") {
        Err(error) => return Err(FailureMode::VarError(error)),
        Ok(token) => token,
    };

    let client = match gitlab::Gitlab::new("gitlab.com", token) {
        Err(error) => return Err(FailureMode::GitlabError(error)),
        Ok(client) => client,
    };

    return Ok(client);
}
