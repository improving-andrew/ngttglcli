use std::borrow::Cow;
use std::fmt;
use std::fmt::Display;

use derive_builder::Builder;
use gitlab::api::common::NameOrId;
use gitlab::api::endpoint_prelude::Method;
use gitlab::api::*;
use serde::{Deserialize, Serialize};

// coverage: off
// See: https://docs.rs/gitlab/latest/gitlab/types/struct.Project.html
#[derive(Serialize, Deserialize, Clone)]
pub struct Project {
    pub name: String,
    pub id: gitlab::ProjectId,
    pub path_with_namespace: String,
}

// See: https://docs.rs/gitlab/latest/gitlab/types/struct.MergeRequest.html
#[derive(Serialize, Deserialize, Clone)]
pub struct MergeRequest {
    pub iid: gitlab::MergeRequestInternalId,
    pub title: String,
}

// overridden because below, in Branch, we only need the Commit title
#[derive(Serialize, Deserialize, Clone)]
pub struct Commit {
    pub title: String,
}

// See: https://docs.rs/gitlab/latest/gitlab/types/struct.RepoBranch.html
#[derive(Serialize, Deserialize, Clone)]
pub struct Branch {
    pub name: String,
    pub commit: Option<Commit>,
}

#[derive(Serialize, Deserialize, Clone)]
pub struct ScanResult {
    pub project: Project,
    pub mrs: Vec<MergeRequest>,
    pub branches: Vec<Branch>,
}

// See: https://docs.rs/gitlab/latest/gitlab/types/struct.Tag.html
#[derive(Serialize, Deserialize, Clone)]
pub struct Tag {
    pub release: Option<ReleaseTag>,
    pub message: Option<String>,
}

#[derive(Serialize, Deserialize, Clone)]
pub struct ReleaseTag {
    pub tag_name: String,
}

impl Display for ReleaseTag {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "[{}]", self.tag_name)
    }
}

/// Query for a specific branch in a project.
#[derive(Debug, Builder, Clone)]
pub struct DeleteBranch<'a> {
    /// The project to get a branch from.
    #[builder(setter(into))]
    project: NameOrId<'a>,
    /// The branch to get.
    #[builder(setter(into))]
    branch: Cow<'a, str>,
}

impl<'a> DeleteBranch<'a> {
    /// Create a builder for the endpoint.
    pub fn builder() -> DeleteBranchBuilder<'a> {
        DeleteBranchBuilder::default()
    }
}

impl<'a> Endpoint for DeleteBranch<'a> {
    fn method(&self) -> Method {
        Method::DELETE
    }

    fn endpoint(&self) -> Cow<'static, str> {
        format!(
            "projects/{}/repository/branches/{}",
            self.project,
            common::path_escaped(&self.branch),
        )
        .into()
    }
}
// coverage: on
