use gitlab::api::*;

use crate::gitlab_api::{DeleteBranch, ScanResult};

pub fn clean_project(client: &gitlab::Gitlab, result: &ScanResult) {
    println!();

    for mr in result.mrs.iter() {
        println!("    Deleting MR #{}: {}", mr.iid, mr.title);

        let endpoint = projects::merge_requests::EditMergeRequest::builder()
            .project(result.project.id.value())
            .merge_request(mr.iid.value())
            .state_event(projects::merge_requests::MergeRequestStateEvent::Close)
            .build()
            .unwrap();

        ignore(endpoint).query(client).unwrap()
    }

    for branch in result.branches.iter() {
        println!(
            "    Deleting branch {} -- \"{}\"",
            branch.name,
            branch
                .commit
                .as_ref()
                .map(|c| c.title.as_str())
                .unwrap_or_else(|| "")
        );

        let endpoint: DeleteBranch = DeleteBranch::builder()
            .project(result.project.id.value())
            .branch(branch.name.clone())
            .build()
            .unwrap();

        ignore(endpoint).query(client).unwrap()
    }
}
