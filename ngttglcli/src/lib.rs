pub use crate::connect::connect;
pub use crate::connect::FailureMode;
pub use crate::fetch::fetch_projects;
pub use crate::gitlab_api::{Branch, MergeRequest, Project, ScanResult, Tag};
pub use crate::scan::{scan_all, scan_without_cache};

mod cache;
pub mod clean_project;
mod connect;
mod fetch;
mod gitlab_api;
mod scan;
pub mod versions;

pub const GROUP_NAME: &str = "cpc-tracktrace";
