use gitlab::api::*;
use regex::Regex;
use version_compare::Version;

use crate::gitlab_api::{ReleaseTag, Tag};
use crate::GROUP_NAME;

pub const SCALA_STEWARD_JIRA_NUMBER: &str = "NGTT-13308";

/// Attempts to display the list of commits that exist on a repo between two version tags.
///
/// Logs an error if the the repo doesn't exist.
// coverage: off
pub fn get_versions(
    repo: &str,
    min_version: &Version,
    max_version: &Version,
    omit_scala_steward: bool,
    client: &gitlab::Gitlab,
) -> String {
    let mut builder = projects::repository::tags::Tags::builder();
    let full_repo_path = format!("{}/{}", GROUP_NAME, repo);
    let endpoint = builder.project(full_repo_path).build().unwrap();

    match paged(endpoint, Pagination::All).query(client) {
        Ok(tags) => {
            fn make_processor<'a>(
                min_version: &'a Version,
                max_version: &'a Version,
                omit_scala_steward: bool,
            ) -> impl Fn(Tag) -> Option<String> + 'a {
                let processor = move |tag: Tag| -> Option<String> {
                    fn reporter(tag_message: &str) -> String {
                        parse_and_format_tag_message(
                            tag_message,
                            &extract_prefix_and_commit_message,
                            &format_prefix_and_commit_message,
                        )
                    }

                    match (tag.release, tag.message) {
                        (Some(release), Some(message)) => process_tag(
                            release,
                            &min_version,
                            &max_version,
                            message.as_str(),
                            omit_scala_steward,
                            &reporter,
                        ),
                        _ => None,
                    }
                };

                return processor;
            }

            process_tags(
                tags,
                &make_processor(min_version, max_version, omit_scala_steward),
            )
            .join("\n\n")
        }

        Err(rest_error) => {
            format!("Repo ReleaseTag Error - {}", rest_error)
        }
    }
}
// coverage: on

fn process_tags(tags: Vec<Tag>, processor: &dyn Fn(Tag) -> Option<String>) -> Vec<String> {
    tags.into_iter()
        .flat_map(|tag| processor(tag))
        .collect::<Vec<String>>()
}

fn process_tag(
    tag_release: ReleaseTag,
    min_version: &Version,
    max_version: &Version,
    tag_message: &str,
    omit_scala_steward: bool,
    reporter: &dyn Fn(&str) -> String,
) -> Option<String> {
    let current_version = match Version::from(tag_release.tag_name.as_str()) {
        None => return None,
        Some(version) => version,
    };

    if (current_version < *min_version) || (current_version > *max_version) {
        return None;
    }

    let tag_report: String = reporter(tag_message);

    // FIXME what if a tag contains both SS and non-SS commits?
    if omit_scala_steward && tag_report.contains(SCALA_STEWARD_JIRA_NUMBER) {
        return None;
    }

    Some(format!("{}\n{}", tag_release, tag_report))
}

fn parse_and_format_tag_message(
    tag_message: &str,
    parser: &dyn Fn(&str) -> Option<(&str, &str)>,
    formatter: &dyn Fn(&str, &str) -> String,
) -> String {
    tag_message
        .lines()
        .flat_map(|line| {
            parser(line).map(|(prefix, commit_message)| formatter(prefix, commit_message))
        })
        .collect::<Vec<String>>()
        .join("\n")
}

fn extract_prefix_and_commit_message(tag_message_line: &str) -> Option<(&str, &str)> {
    let commit_message_regex = Regex::new(r#"Commit\([^,]+,(?:(.*):)?(.*)\)"#).unwrap();

    match commit_message_regex.captures(tag_message_line) {
        None => None,
        Some(captures) => {
            let prefix = captures.get(1).map(|m| m.as_str()).unwrap_or_default();
            let commit_message = captures
                .get(2)
                .map(|m| m.as_str().trim())
                .unwrap_or_default();
            Some((prefix, commit_message))
        }
    }
}

fn format_prefix_and_commit_message(prefix: &str, commit_message: &str) -> String {
    if commit_message.contains(SCALA_STEWARD_JIRA_NUMBER) {
        format!("🤖 {}:{}", prefix, commit_message)
    } else if prefix == "fix" {
        format!("🐞 {}:{}", prefix, commit_message)
    } else if prefix == "BREAKING CHANGE" || prefix == "BREAKING-CHANGE" {
        format!("⚠️ {}:{}", prefix, commit_message)
    } else {
        format!("✅ {}:{}", prefix, commit_message)
    }
}

// coverage: off
#[cfg(test)]
mod tests {
    use crate::versions::*;
    use version_compare::Version;

    #[test]
    fn format_fix_commits() {
        let prefix = "fix";
        let commit_message = "my commit message here";
        let formatted = format_prefix_and_commit_message(prefix, commit_message);

        assert_eq!(formatted, "🐞 fix:my commit message here")
    }

    #[test]
    fn format_feat_commits() {
        let prefix = "feat";
        let commit_message = "my commit message here";
        let formatted = format_prefix_and_commit_message(prefix, commit_message);

        assert_eq!(formatted, "✅ feat:my commit message here")
    }

    #[test]
    fn format_breaking_change_commits() {
        let prefix = "BREAKING CHANGE";
        let commit_message = "my commit message here";
        let formatted = format_prefix_and_commit_message(prefix, commit_message);

        assert_eq!(formatted, "⚠️ BREAKING CHANGE:my commit message here")
    }

    #[test]
    fn format_breaking_hyphen_change_commits() {
        let prefix = "BREAKING-CHANGE";
        let commit_message = "my commit message here";
        let formatted = format_prefix_and_commit_message(prefix, commit_message);

        assert_eq!(formatted, "⚠️ BREAKING-CHANGE:my commit message here")
    }

    #[test]
    fn format_scala_steward_commits() {
        let prefix = "this can be anything";
        let commit_message = "this is also arbitrary [NGTT-13308]";
        let formatted = format_prefix_and_commit_message(prefix, commit_message);

        assert_eq!(
            formatted,
            "🤖 this can be anything:this is also arbitrary [NGTT-13308]"
        )
    }

    #[test]
    fn format_other_commits() {
        let prefix = "something else";
        let commit_message = "doesn't contain Scala Steward Jira number";
        let formatted = format_prefix_and_commit_message(prefix, commit_message);

        assert_eq!(
            formatted,
            "✅ something else:doesn't contain Scala Steward Jira number"
        )
    }

    #[test]
    fn extract_tag_prefix_and_message() {
        let tag_message_line = "Commit(b511d837,feat: Add new feature to international-service-standards-api [NGTT-13308])";

        let actual = extract_prefix_and_commit_message(tag_message_line);

        match actual {
            None => panic!("expected valid tag message line to be parsed correctly"),
            Some((prefix, commit_message)) => {
                assert_eq!(prefix, "feat");
                assert_eq!(
                    commit_message,
                    "Add new feature to international-service-standards-api [NGTT-13308]"
                )
            }
        }
    }

    #[test]
    fn not_panic_on_invalid_tag_message_lines() {
        let tag_message_line = "Whoops(b511d837,feat: Add new feature to international-service-standards-api [NGTT-13308])";

        let actual = extract_prefix_and_commit_message(tag_message_line);

        match actual {
            None => (),
            Some(_) => panic!("expected invalid tag message line to return None"),
        }
    }

    #[test]
    fn parse_and_format_tag_messages() {
        let commit_message = "\
            Commit(b511d837,feat: Add new feature to international-service-standards-api [NGTT-13308])
            Commit(b511d837,fix: Add new feature to international-service-standards-api [NGTT-13308])
            Commit(b511d837,fix: Update induction-client from 7.11.17 to 7.12.2 [NGTT-13308])
            Commit(b511d837,fix: Update sbt from 1.4.9 to 1.8.0 [NGTT-13308])";

        fn formatter(prefix: &str, commit_message: &str) -> String {
            format!("{} // {}", commit_message.trim(), prefix.trim())
        }

        fn parser(tag_message_line: &str) -> Option<(&str, &str)> {
            tag_message_line.split_once(":")
        }

        let actual = parse_and_format_tag_message(commit_message, &parser, &formatter);

        let expected = String::from("\
            Add new feature to international-service-standards-api [NGTT-13308]) // Commit(b511d837,feat\n\
            Add new feature to international-service-standards-api [NGTT-13308]) // Commit(b511d837,fix\n\
            Update induction-client from 7.11.17 to 7.12.2 [NGTT-13308]) // Commit(b511d837,fix\n\
            Update sbt from 1.4.9 to 1.8.0 [NGTT-13308]) // Commit(b511d837,fix");

        assert_eq!(actual, expected)
    }

    #[test]
    fn ignore_tags_outside_version_range() {
        fn reporter(_ignored: &str) -> String {
            String::from("called reporter")
        }

        let v2 = Version::from("2.0.0").unwrap();
        let v3 = Version::from("3.0.0").unwrap();

        let below_min_version = process_tag(
            ReleaseTag {
                tag_name: String::from("1.0.0"),
            },
            &v2,
            &v3,
            "",
            false,
            &reporter,
        );

        assert_eq!(below_min_version, None);

        let above_max_version = process_tag(
            ReleaseTag {
                tag_name: String::from("4.0.0"),
            },
            &v2,
            &v3,
            "",
            false,
            &reporter,
        );

        assert_eq!(above_max_version, None);

        let within_range = process_tag(
            ReleaseTag {
                tag_name: String::from("2.5.0"),
            },
            &v2,
            &v3,
            "",
            false,
            &reporter,
        );

        assert_eq!(within_range, Some(String::from("[2.5.0]\ncalled reporter")));
    }

    #[test]
    fn omit_scala_steward_when_instructed() {
        fn reporter(tag_message: &str) -> String {
            String::from(tag_message)
        }

        let v2 = Version::from("2.0.0").unwrap();
        let v3 = Version::from("3.0.0").unwrap();

        let with_scala_steward = process_tag(
            ReleaseTag {
                tag_name: String::from("2.5.0"),
            },
            &v2,
            &v3,
            "tag message [NGTT-13308]",
            false,
            &reporter,
        );

        assert_eq!(
            with_scala_steward,
            Some(String::from("[2.5.0]\ntag message [NGTT-13308]"))
        );

        let without_scala_steward = process_tag(
            ReleaseTag {
                tag_name: String::from("2.5.0"),
            },
            &v2,
            &v3,
            "tag message [NGTT-13308]",
            true,
            &reporter,
        );

        assert_eq!(without_scala_steward, None);
    }

    #[test]
    fn handle_unparseable_tags() {
        fn reporter(tag_message: &str) -> String {
            String::from(tag_message)
        }

        let v2 = Version::from("2.0.0").unwrap();
        let v3 = Version::from("3.0.0").unwrap();

        let weird_version = process_tag(
            ReleaseTag {
                tag_name: String::from("this is an unusual version tag"),
            },
            &v2,
            &v3,
            "tag message [NGTT-13308]",
            false,
            &reporter,
        );

        assert_eq!(weird_version, None);
    }

    #[test]
    fn process_tags_by_invoking_the_processor_for_each_tag() {
        fn processor(tag: Tag) -> Option<String> {
            tag.message.map(|s| format!("{}!", s.to_uppercase()))
        }

        let tags: Vec<Tag> = vec![
            Tag {
                release: None,
                message: Some(String::from("veni")),
            },
            Tag {
                release: None,
                message: Some(String::from("vidi")),
            },
            Tag {
                release: None,
                message: Some(String::from("vici")),
            },
        ];

        let actual = process_tags(tags, &processor);

        assert_eq!(actual, vec!["VENI!", "VIDI!", "VICI!"])
    }
}
// coverage: on
