use std::collections::HashMap;
use std::time::Duration;

use gitlab::api::*;

use crate::cache::{overwrite_cache, read_from_cache, update_cache};
pub use crate::gitlab_api::{Branch, MergeRequest, Project, ScanResult, Tag};
use crate::GROUP_NAME;

pub const PROJECTS_CACHE_FILE_NAME: &str = ".ngttglcli_projects";

pub const PROJECTS_CACHE_MAX_AGE_SECONDS: u64 = 60 * 60 * 24;

/// Queries GitLab for all projects under the GROUP_NAME and its subgroups.
///
/// If the `search` term is not [None], only the subset of projects which match that search term will be fetched.
fn fetch_projects_without_cache(
    client: &gitlab::Gitlab,
    search: Option<&str>,
) -> HashMap<String, Project> {
    let mut builder = groups::projects::GroupProjects::builder();
    let builder = builder.group(GROUP_NAME).include_subgroups(true);

    if let Some(term) = search {
        builder.search(term);
    }

    let pageable_endpoint = builder.build().unwrap();

    let projects: Vec<Project> = paged(pageable_endpoint, Pagination::All)
        .query(client)
        .unwrap();

    projects.into_iter().map(|p| (p.name.clone(), p)).collect()
}

/// Uses the cache to return a list of [Project]s.
///
/// If `force_cache_update` is `true`, all projects will be fetched from GitLab, and the cache will be overwritten with that information.
///
/// Otherwise, if the search term is not [None], only the subset of projects which match that search term will be fetched from GitLab, and the cache will be updated with that information. The refresh time of the cache will be unaffected.
///
/// Otherwise, if the cache cannot be read, all projects will be fetched from GitLab.
///
/// Otherwise, the cache will be returned.
pub fn fetch_projects(
    client: &gitlab::Gitlab,
    force_cache_update: bool,
    search: Option<&str>,
) -> HashMap<String, Project> {
    if force_cache_update {
        println!("Performing a forced cache update of the list of projects...");
        let projects = fetch_projects_without_cache(client, None);
        overwrite_cache(PROJECTS_CACHE_FILE_NAME, &projects).expect("Failed to write to cache");
        projects
    } else if let Some(_) = search {
        let projects = fetch_projects_without_cache(client, search);
        update_cache(
            PROJECTS_CACHE_FILE_NAME,
            Duration::from_secs(PROJECTS_CACHE_MAX_AGE_SECONDS),
            &projects,
        );
        projects
    } else {
        read_from_cache(
            PROJECTS_CACHE_FILE_NAME,
            Duration::from_secs(PROJECTS_CACHE_MAX_AGE_SECONDS),
        )
        .unwrap_or_else(|error| {
            eprintln!("Failed to read projects from cache due to: {}", error);
            let projects = fetch_projects_without_cache(client, None);
            overwrite_cache(PROJECTS_CACHE_FILE_NAME, &projects).expect("Failed to write to cache");
            projects
        })
    }
}

/// Queries GitLab for any open Scala Steward MRs for the specified project.
pub fn fetch_open_scala_steward_mrs(
    client: &gitlab::Gitlab,
    project: &Project,
) -> Vec<MergeRequest> {
    let pageable_endpoint = projects::merge_requests::MergeRequests::builder()
        .project(project.id.value())
        .search("NGTT-13308") // this is how we distinguish Scala Steward MRs
        .state(projects::merge_requests::MergeRequestState::Opened)
        .build()
        .unwrap();

    paged(pageable_endpoint, Pagination::All)
        .query(client)
        .unwrap()
}

/// Queries GitLab for any Scala Steward branches for the specified project.
pub fn fetch_scala_steward_branches(client: &gitlab::Gitlab, project: &Project) -> Vec<Branch> {
    let pageable_endpoint = projects::repository::branches::Branches::builder()
        .project(project.id.value())
        .search("update/") // this is how we distinguish Scala Steward branches
        .build()
        .unwrap();

    paged(pageable_endpoint, Pagination::All)
        .query(client)
        .unwrap()
}
