use std::collections::HashMap;
use std::io::Write;
use std::time::Duration;

use crate::cache::{overwrite_cache, read_from_cache, update_cache};
use crate::fetch::{fetch_open_scala_steward_mrs, fetch_projects, fetch_scala_steward_branches};
pub use crate::gitlab_api::{Branch, MergeRequest, Project, ScanResult, Tag};

pub const SCAN_CACHE_FILE_NAME: &str = ".ngttglcli_scan";

pub const SCAN_CACHE_MAX_AGE_SECONDS: u64 = 60 * 60 * 24;

/// Queries GitLab for any Scala Steward branches and open Scala Steward MRs for the specified project and returns them in a [ScanResult].
pub fn scan_without_cache(client: &gitlab::Gitlab, project: &Project) -> ScanResult {
    let mrs = fetch_open_scala_steward_mrs(client, project);
    let branches = fetch_scala_steward_branches(client, project);

    ScanResult {
        project: project.clone(),
        mrs,
        branches,
    }
}

/// [Scan](scan_without_cache)s all projects.
///
/// If `force_project_cache_update` is set to `true`, the list of projects will be re-fetched before scanning.
fn scan_all_without_cache(
    client: &gitlab::Gitlab,
    force_project_cache_update: bool,
    search: Option<&str>,
) -> HashMap<String, ScanResult> {
    let projects = fetch_projects(client, force_project_cache_update, search);
    let n_projects = projects.len();

    print!("Progress: |{}|", " ".repeat(n_projects));
    std::io::stdout().flush().unwrap(); // if the buffer doesn't flush, then... ???

    let results = projects
        .values()
        .enumerate()
        .map(|(index, project)| {
            print!(
                "\rProgress: |{}{}|",
                "=".repeat(index + 1),
                " ".repeat(n_projects - (index + 1))
            );
            std::io::stdout().flush().unwrap(); // if the buffer doesn't flush, then... ???
            project
        })
        .map(|project| scan_without_cache(client, project))
        .collect::<Vec<ScanResult>>();

    println!();
    results
        .into_iter()
        .map(|r| (r.project.id.value().to_string(), r))
        .collect()
}

/// Uses the caches to scan a list of [Project]s.
///
/// If `force_scan_cache_update` is `true`, all branches and MRs will be fetched from GitLab, and the cache will be overwritten with that information.
///
/// Otherwise, if the `search` term is not [None], only the subset of projects which match that search term will be fetched from GitLab, and the cache will be updated with that information. The refresh time of the cache will be unaffected.
///
/// Otherwise, if the cache cannot be read, all branches and MRs will be fetched from GitLab.
///
/// Otherwise, the cache will be returned.
pub fn scan_all(
    client: &gitlab::Gitlab,
    force_project_cache_update: bool,
    force_scan_cache_update: bool,
    search: Option<&str>,
) -> HashMap<String, ScanResult> {
    if force_scan_cache_update {
        println!("Performing a forced cache update of the list of scan results...");
        let scan_results = scan_all_without_cache(client, force_project_cache_update, None);
        overwrite_cache(SCAN_CACHE_FILE_NAME, &scan_results).expect("Failed to write to cache");
        scan_results
    } else if let Some(_) = search {
        let scan_results = scan_all_without_cache(client, force_project_cache_update, search);
        update_cache(
            SCAN_CACHE_FILE_NAME,
            Duration::from_secs(SCAN_CACHE_MAX_AGE_SECONDS),
            &scan_results,
        );
        scan_results
    } else {
        read_from_cache(
            SCAN_CACHE_FILE_NAME,
            Duration::from_secs(SCAN_CACHE_MAX_AGE_SECONDS),
        )
        .unwrap_or_else(|error| {
            eprintln!("Failed to read scan results from cache due to: {}", error);
            let scan_results = scan_all_without_cache(client, force_project_cache_update, None);
            overwrite_cache(SCAN_CACHE_FILE_NAME, &scan_results).expect("Failed to write to cache");
            scan_results
        })
    }
}
