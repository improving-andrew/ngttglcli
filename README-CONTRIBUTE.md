# `ngttglcli` Contributor Guide

## One-Time Contributor Setup

Before you can contribute to this project, you'll need to run a few setup tasks.

### Make sure you installed `rust` via `rustup`.

This is necessary because some `rust` compilers are configured differently than `rustup`s, and this can cause unexpected problems, like those seen in the [Cross-Compiling](README-DEV.md#cross-compiling) section of [README-DEV.md](README-DEV.md).

On Macs, to uninstall `homebrew`'s `rust`, you can do

```shell
brew uninstall rust
```

To install `rust` via `rustup`, follow the instructions at https://rustup.rs/.

### Make sure you have the necessary formatting and testing dependencies.

You can get these by running

```shell
rustup component add rustfmt
cargo install cargo-llvm-cov
cargo install grcov
rustup component add llvm-tools-preview
```

You might also need

```shell
cargo install profiler_builtins
```

Note that these are the same dependencies used by the pipeline in `.gitlab-ci.yml`, they are required for running a code format check and code coverage analysis.

### Make sure you set up the git hooks.

Do this by running

```shell
git config --local core.hooksPath .githooks
```

This uses the hooks defined in `.githooks` for this repo, rather than the default location (`.git/hooks`), which cannot be added to version control.

Doing this means that the `pre-push` hook will be run before you can push to a branch. The `pre-push` hook is configured to do a formatting check with `rustfmt` and a code coverage check with `grcov` (which is why we installed those dependencies above).

## Creating a Merge Request

Create a local feature branch with

```shell
git switch -c my-feature-branch
```

Make your changes and `git add` them to staging, then `git commit` them to the branch. When you're ready to create a merge request, `git push` the changes from your local feature branch to the remote version of that branch

```shell
git push --set-upstream origin my-feature-branch
```

The `git` CLI will give you a link similar to the following

```
remote: 
remote: To create a merge request for my-feature-branch, visit:
remote:   https://gitlab.com/awwsmm/ngttglcli/-/merge_requests/new?merge_request%5Bsource_branch%5D=my-feature-branch
remote: 
```

Click on that link to create a merge request.

The page it directs you to should look similar to the following

![](images/new-merge-request.png)

The merge request should be requesting to merge the branch `<username>/<feature-branch>` into `improving-andrew/ngttglcli:master`. If this is not correct, click <kbd>Change branches</kbd> and select the appropriate source and target branches.

Click <kbd>Create merge request</kbd> to complete the process. This will create a new merge request and start the CI pipeline.

If you see a warning in your pipeline like

> **User validation required**
>
> To use free compute minutes on shared runners, you'll need to validate your account with a credit card. ...

You will need to follow those instructions in order to run the pipeline. Note that GitLab won't charge your credit card, this is just meant to discourage abuse by bots, etc.