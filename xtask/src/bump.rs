use std::{fs, io};

pub enum VersionBump {
    Major,
    Minor,
    Patch,
}

pub fn version_bump(bump: VersionBump) -> io::Result<()> {
    fn update_version(path: &str, bump: &VersionBump) -> io::Result<()> {
        let version_line_regex =
            regex::Regex::new(r#"^version = "(?<major>\d+)\.(?<minor>\d+)\.(?<patch>\d+)"$"#)
                .unwrap();

        let mut xtask_cargo_toml = fs::read_to_string(path)?;

        xtask_cargo_toml = xtask_cargo_toml
            .lines()
            .into_iter()
            .map(|line| {
                version_line_regex
                    .captures(line)
                    .map(|captures| {
                        let mut major: u64 =
                            captures.name("major").unwrap().as_str().parse().unwrap();
                        let mut minor: u64 =
                            captures.name("minor").unwrap().as_str().parse().unwrap();
                        let mut patch: u64 =
                            captures.name("patch").unwrap().as_str().parse().unwrap();

                        match bump {
                            &VersionBump::Major => {
                                major += 1;
                                minor = 0;
                                patch = 0;
                            }
                            &VersionBump::Minor => {
                                minor += 1;
                                patch = 0;
                            }
                            &VersionBump::Patch => patch += 1,
                        }

                        format!("version = \"{}.{}.{}\"", major, minor, patch)
                    })
                    .unwrap_or_else(|| line.to_string())
            })
            .collect::<Vec<String>>()
            .join("\n");

        fs::write(path, xtask_cargo_toml)
    }

    update_version("xtask/Cargo.toml", &bump)?;
    update_version("ngttglcli/Cargo.toml", &bump)
}
