use std::{env, fs, io};

use serde::Deserialize;

use crate::bump::{version_bump, VersionBump};

mod bump;
mod coverage;

#[derive(Deserialize)]
struct Config {
    workspace: Workspace,
}

#[derive(Deserialize)]
struct Workspace {
    members: Vec<String>,
}

fn main() -> io::Result<()> {
    let args: Vec<String> = env::args().collect();

    match args.get(1) {
        None => Err(io::Error::new(
            io::ErrorKind::InvalidInput,
            "missing command (usage: cargo xtask <command>)",
        )),

        Some(command) => match command.as_str() {
            "coverage" => match args.get(2) {
                None => Err(io::Error::new(
                    io::ErrorKind::InvalidInput,
                    "missing crate name (usage: cargo xtask coverage <crate_name>)",
                )),

                Some(crate_name) => {
                    let cargo_toml = fs::read_to_string("Cargo.toml")?;
                    let config = toml::from_str::<Config>(&cargo_toml).unwrap();

                    if config.workspace.members.contains(crate_name) {
                        coverage::coverage(crate_name)
                    } else {
                        Err(io::Error::new(
                            io::ErrorKind::InvalidInput,
                            format!("unknown crate name: {crate_name}"),
                        ))
                    }
                }
            },

            "bump" => match args.get(2) {
                None => Err(io::Error::new(
                    io::ErrorKind::InvalidInput,
                    "missing version bump (usage: cargo xtask bump {major,minor,patch})",
                )),

                Some(version) => {
                    match version.as_str() {
                        "major" => version_bump(VersionBump::Major),
                        "minor" => version_bump(VersionBump::Minor),
                        "patch" => version_bump(VersionBump::Patch),
                        _ => Err(io::Error::new(
                            io::ErrorKind::InvalidInput,
                            format!("invalid version bump \"{version}\", expected one of: \"major\", \"minor\", \"patch\""),
                        ))
                    }
                }
            },

            unknown => Err(io::Error::new(
                io::ErrorKind::NotFound,
                format!("unknown command: {unknown}"),
            )),
        },
    }
}
