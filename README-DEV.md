# `ngttglcli` Development Guide

## One-Time Setup

If you do not want to package the code for distribution (you probably don't, unless you're me), then the only subsections you need to follow from this section are "Fork `ngttglcli`" and "Cargo". You can skip down to the "Building and Running" section.

If you want to create executables to be run on other machines, you will need to follow all subsections here.

### Fork `ngttglcli`

You can clone this parent repo directly, but contributions are made to this repo by forking it, creating a feature branch in your fork, and then creating a merge request to bring those changes in your branch into this repo's `master` branch. So it's recommended that you fork this repo and clone the fork, rather than cloning this repo directly.

> See: [What is a fork?](https://docs.github.com/en/get-started/quickstart/fork-a-repo)

You only need to create the fork once. If you make more than one contribution, successive contributions will need their own feature branches and merge requests, but you can reuse the same fork.

Click <kbd>Forks</kbd> in the top right corner of repo to create a new fork

![Screenshot of the "Forks" button in the GitLab UI.](images/create-new-fork.png)

This will open a page which looks similar to the following

![](images/fork-project.png)

Select your namespace (for your username) and make sure the fork has **Public** visibility. Then click <kbd>Fork project</kbd>.

You'll be taken to the page for your newly-forked repo. Download the code by clicking the <kbd>Clone</kbd> button in the UI, as shown below

![Screenshot of the "Clone" button in the GitLab UI.](images/clone.png)

Once you have a local copy of the forked repo, you can create feature branches and merge requests for those branches.

### Cargo

This repo can be compiled from source with Cargo, the Rust build tool.

Cargo is also Rust's package ("crate") manager.

You can [install Cargo (and `rustup`, the Rust installer) with](https://doc.rust-lang.org/cargo/getting-started/installation.html)

```shell
curl https://sh.rustup.rs -sSf | sh
```

Verify that Cargo was installed correctly with

```shell
cargo --version
```

The output should look something like

```
cargo 1.70.0 (ec8a8a0ca 2023-04-25)
```

Try building this repo by running the following command in the same directory as this `README`

```shell
cargo build
```

### Cross

If you want to cross-compile this repo to create executables which can be run on Intel and Apple Silicon machines, you'll need to install `cross` next.

`cross` is a Rust crate for cross-compilation. [Install it with](https://github.com/cross-rs/cross#installation)

```shell
cargo install cross --git https://github.com/cross-rs/cross
```

This will install `cross` in your `cargo` installation's `bin` directory (usually `$HOME/.cargo/bin`).

### macOS SDK

Cross requires Docker container images built with macOS SDKs to do its cross-compilation.

You can [build one of these from source](https://github.com/tpoechtrager/osxcross#packaging-the-sdk) as well, but there are also prepackaged macOS SDKs available online, [like at this repo](https://github.com/joseluisq/macosx-sdks).

Grab the URL of the `tar.xz` file for one of the SDKs listed above and save it in an environment variable, like

```shell
export MACOS_SDK_URL="https://github.com/joseluisq/macosx-sdks/releases/download/12.3/MacOSX12.3.sdk.tar.xz"
```

We will use this in the next step.

> Note: As of this writing, SDK `12.3` from the above repo is known to work, but
>
> - `13.0` fails with `mv: cannot stat '*OSX*13.0*sdk*': No such file or directory`
> - `13.1` fails with an `Unsupported SDK` error

### Building the Docker Container Images

Next, to create the required Docker container images, you'll also need to [clone the `cross` repo](https://github.com/cross-rs/cross-toolchains#cross-toolchains)

```shell
cd $HOME
git clone https://github.com/cross-rs/cross
cd cross
git submodule update --init --remote
```

Still in the `cross/` directory, build the required images with

```shell
# create M1 container image (can take ~40m to build)
cargo build-docker-image aarch64-apple-darwin-cross --tag local --build-arg MACOS_SDK_URL=$MACOS_SDK_URL
```

```shell
# create Intel container image (fast if layers built above have been cached)
cargo build-docker-image x86_64-apple-darwin-cross --tag local --build-arg MACOS_SDK_URL=$MACOS_SDK_URL
```

Note that these container images are required by the `Cross.toml` file in this project

```toml
[target.x86_64-apple-darwin]
image = "ghcr.io/cross-rs/x86_64-apple-darwin-cross:local"

[target.aarch64-apple-darwin]
image = "ghcr.io/cross-rs/aarch64-apple-darwin-cross:local"
```

Verify that the container images have been correctly created

```shell
docker image list | grep ghcr.io
```

## Building and Running

This project can be built with

```shell
cargo build
```

or built and run with

```shell
cargo run
```

## Publishing

### Cut a New Release

On a clean `master` branch, run the `release.sh` script with whatever version bump (`patch`, `minor`, `major`) you think is appropriate. For example

```shell
chmod +x release.sh
./release.sh patch
```

This will

1. update the version numbers in the `ngttglcli` and `xtask` accordingly
2. create three executables: for Intel Macs, Apple Silicon Macs, and Windows PCs
3. create a new `git` branch for the release
4. package the Mac executables into `.tar.gz` tarball archives
5. calculate and print the SHAs of those tarballs -- make a note of these, you'll need them later
6. `commit` all changes to the new branch and `push` to the remote repo

The `git` CLI will give you a link similar to the following

```
remote:
remote: To create a merge request for 0.1.3, visit:
remote:   https://gitlab.com/awwsmm/ngttglcli/-/merge_requests/new?merge_request%5Bsource_branch%5D=0.1.3
remote:
```

...click on that hyperlink to create a Merge Request.

Once the pipeline passes for the MR, it can be merged into the `master` branch.

### Publish to GitLab

Next, create a new release on GitLab at [gitlab.com/improving-andrew/ngttglcli/-/releases](https://gitlab.com/improving-andrew/ngttglcli/-/releases), making sure to add the **permalink** URL of the tarballs and the Windows executable to the "Release assets" section at the bottom. See an [example release here](https://gitlab.com/improving-andrew/ngttglcli/-/releases/0.1.3). Set the "Type" of each release asset to "Package".

Make a note of the commit SHA for this release. In this example (`0.1.3`), [this is](https://gitlab.com/improving-andrew/ngttglcli/-/commit/46393cb7f86a5009b09cedc72403d3d7203f7712)

```
46393cb7f86a5009b09cedc72403d3d7203f7712
```

### Publish to Homebrew

Next, update the `ngttglcli.rb` Homebrew formula at [gitlab.com/cpc-tracktrace/homebrew-ngtt/-/blob/master/Formula/ngttglcli.rb](https://gitlab.com/cpc-tracktrace/homebrew-ngtt/-/blob/master/Formula/ngttglcli.rb) with

- the new version number
- the new git commit SHA
- the SHA for each tarball

Once that change is merged into `master` at `homebrew-ngtt`, you can run

```shell
brew update
brew upgrade ngttglcli
```

...to get the updated version locally.

## Testing

Calculate test coverage (and generate an HTML report) with

```shell
cargo coverage
```

## Formatting

Check that code is formatted correctly with

```shell
cargo fmt --all -- --check
```

Reformat code with

```shell
cargo fmt
```

## Contributing

Next, read the [Contributor Guide](README-CONTRIBUTE.md) to learn how to contribute your changes to `ngttglcli`.